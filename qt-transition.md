# Notes on doing a Qt transititon

Currently we always lack behind in Qt transitions, and everything tends to break when debian does one.
For sure they can't and won't wait for debian-pm to rebuild our packages, so we will have to make sure our packages are not overriden by debian ones.
Thanks to our apt pin, it will just refuse to update until debian-pm has updated Qt.

## Upgrading Qt

Merge in the debian/debian/$QT_VERSION-$DEBIAN_REV tag in the qtbase package.
Wait for all builds to complete on GitLab CI, and make sure they are available in the repository.
Then merge the tags for qtdeclarative and qtmultimedia.
After the Qt builds itself are finished and published, kwin needs to be rebuilt as well.
Since upstream debian needs to do the same, have a look at their version number and use the same.

Provided we manage to improve our build infrastructure a lot, this should be possible to do in about a day.
