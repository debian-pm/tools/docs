# List of forked packages

## [ofono](https://gitlab.com/debian-pm/ofono):
- added ofono-voicecall patches (required for modem and calling on mainline)

## [qtbase](https://gitlab.com/debian-pm/qtbase):
- enabled GL es on i386 and arm64
- disabled features depending on kernel 3.16+ (we support devices with older kernels)

Read about how to manage this package in qt-transiton.md

## [qtdeclarative](https://gitlab.com/debian-pm/qtdeclarative):
- Changed and recompiled to work with GL es qtbase

Read about how to manage this package in qt-transiton.md

## [qtmultimedia](https://gitlab.com/debian-pm/qtmultimedia):
- Changed and recompiled to work with GL es qtbase

Read about how to manage this package in qt-transiton.md

## [alsa-ucm-conf](https://gitlab.com/debian-pm/mainline/alsa-ucm-conf):
- From mobian, adds patch for audio on the Pine Phone.
